import java.util.HashMap;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import static spark.Spark.*;


public class App {
  public static void main(String[] args) {
    staticFileLocation("/public");
    String layout = "templates/layout.vtl";

    get("/", (request, response) -> {
        HashMap model = new HashMap();
        model.put("template", "templates/index.vtl" );
        model.put("words", Word.getAllWords());
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());

    post("/", (request, response) -> {
        HashMap model = new HashMap();
        String name = request.queryParams("word");
        String definitionText = request.queryParams("definition");
        if (definitionText != null && definitionText.length() > 0) {
          Definition newDefinition = new Definition(definitionText);
          Word newWord = new Word(name, newDefinition);
        } else {
          Word newWord = new Word(name);
        }
        model.put("words", Word.getAllWords());
        model.put("template", "templates/index.vtl" );
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());


    get("/word/new", (request, response) -> {
        HashMap model = new HashMap();
        model.put("template", "templates/new-word.vtl" );
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());


    get("/word/:id", (request, response) -> {
        HashMap model = new HashMap();
        Word newWord = Word.findByID(Integer.parseInt(request.params(":id")));
        if (newWord != null) {
          model.put("definitions", newWord.getDefinitions());
          model.put("word", newWord);
        }
        model.put("template", "templates/word.vtl");
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());

    post("/word/:id", (request, response) -> {
        HashMap model = new HashMap();
        Word newWord = Word.findByID(Integer.parseInt(request.params(":id")));
        System.out.println(Integer.parseInt(request.params(":id")));
        Definition newDefinition = new Definition(request.queryParams("definition"));
        newWord.addDefinition(newDefinition);
        model.put("definitions", newWord.getDefinitions());
        model.put("word", newWord);
        model.put("template", "templates/word.vtl");
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());

    get("/word/:id/define", (request, response) -> {
        HashMap model = new HashMap();
        Word newWord = Word.findByID(Integer.parseInt(request.params(":id")));
        model.put("definitions", newWord.getDefinitions());
        model.put("word", newWord);
        model.put("template", "templates/define.vtl");
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());


  }
}
