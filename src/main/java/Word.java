import java.util.ArrayList;

public class Word {
  private String mWordName;
  private int mID;
  private ArrayList<Definition> mDefinitions = new ArrayList<Definition>(); //When I tried declaring this ArrayList without calling the constructor (i.e., "private ArrayList<Definition> mDefinitions;"), I would get a nullPointerException when attempting to manupilate this object. Why?
  private static ArrayList<Word> wordArray = new ArrayList<Word>();

  public Word(String wordName){
    this(wordName, null); //is this teh right way to do this?
  }

  public Word(String wordName, Definition definition){
    mWordName = wordName;
    mDefinitions.add(definition);
    wordArray.add(this);
    mID = wordArray.size();
  }

  public static ArrayList<Word> getAllWords() {
    return wordArray;
  }

  public static void clearWordArray() {
    wordArray.clear();
  }

  public static Word findByID(int id) {
    try {
      return wordArray.get(id - 1);
    } catch (IndexOutOfBoundsException e) {
      return null;
    }
  }

  public String getWordName() {
    return mWordName;
  }

  public int getID() {
    return mID;
  }

  public ArrayList<Definition> getDefinitions() {
    return mDefinitions;
  }

  public void addDefinition(Definition definition) {
    mDefinitions.add(definition);
  }




}
