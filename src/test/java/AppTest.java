import org.fluentlenium.adapter.FluentTest;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import java.io.*;

import static org.fluentlenium.core.filter.FilterConstructor.*;

import static org.assertj.core.api.Assertions.assertThat;

public class AppTest extends FluentTest {
  public WebDriver webDriver = new HtmlUnitDriver();

  @Override
  public WebDriver getDefaultDriver() {
    return webDriver;
  }

  @ClassRule
  public static ServerRule server = new ServerRule();

  @After
  public void tearDown() {
    Word.clearWordArray();
  }

  @Test
  public void rootTest() {
    goTo("http://localhost:4567/");
    assertThat(pageSource()).contains("Dictionary");
  }

  @Test
  public void newWordTest() {
    goTo("http://localhost:4567/word/new");
    assertThat(pageSource()).contains("Add a New Word:");
  }

  @Test
  public void addWord() {
    goTo("http://localhost:4567/");
    click("a", withText("Add word."));
    fill("#word").with("testWord");
    fill("#definition").with("testDefinition");
    submit(".btn");
    assertThat(pageSource()).contains("testWord");
  }

  @Test
  public void indexShowsMultipleWords() {
    goTo("http://localhost:4567/");
    click("a", withText("Add word."));
    fill("#word").with("testWord");
    submit(".btn");
    click("a", withText("Add word."));
    fill("#word").with("testWord2");
    submit(".btn");
    assertThat(pageSource()).contains("testWord");
    assertThat(pageSource()).contains("testWord2");
  }

  @Test
  public void viewWord() {
    goTo("http://localhost:4567/");
    click("a", withText("Add word."));
    fill("#word").with("testWord");
    submit(".btn");
    click("a", withText("testWord"));
    assertThat(pageSource()).contains("testWord");
  }

  @Test
  public void viewWordShowsDefinition() {
    goTo("http://localhost:4567/");
    click("a", withText("Add word."));
    fill("#word").with("testWord");
    fill("#definition").with("Test definition");
    submit(".btn");
    click("a", withText("testWord"));
    assertThat(pageSource()).contains("Test definition");
  }

  @Test
  public void viewWordShowsMessageIfNoDefinitions() {
    goTo("http://localhost:4567/");
    click("a", withText("Add word."));
    fill("#word").with("testWord");
    submit(".btn");
    click("a", withText("testWord"));
    assertThat(pageSource()).contains("Word not defined");
  }

  @Test
  public void defineAddsDefinitionToWord() {
    goTo("http://localhost:4567/");
    click("a", withText("Add word."));
    fill("#word").with("testWord");
    fill("#definition").with("Test definition");
    submit(".btn");
    click("a", withText("testWord"));
    click("a", withText("Add definition to testWord"));
    fill("#definition").with("Test definition2");
    submit(".btn");
    assertThat(pageSource()).contains("Test definition");
  }

  @Test
  public void defineShowsExistingDefinitions() {
    goTo("http://localhost:4567/");
    click("a", withText("Add word."));
    fill("#word").with("testWord");
    fill("#definition").with("Test definition");
    submit(".btn");
    click("a", withText("testWord"));
    click("a", withText("Add definition to testWord"));
    assertThat(pageSource()).contains("Test definition");
  }

  @Test
  public void wordNotFoundMessageShown() {
    goTo("http://localhost:4567/word/999");
    assertThat(pageSource()).contains("Word not found.");
  }

}
