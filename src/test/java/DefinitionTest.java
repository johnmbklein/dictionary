import org.junit.*;
import static org.junit.Assert.*;
import java.io.*;
import java.util.ArrayList;


public class DefinitionTest {

  @Test
  public void Definition_instantiatesCorrectly_true(){
    Definition testDefinition = new Definition("Test");
    assertTrue(testDefinition instanceof Definition);
  }

  @Test
  public void Definition_instantiatesWithText_true(){
    Definition testDefinition = new Definition("Text");
    assertEquals("Text", testDefinition.getText());
  }

  @Test
  public void getText_returnsText_returnText(){
    Definition testDefinition = new Definition("returnText");
    assertEquals("returnText", testDefinition.getText());
  }

}
