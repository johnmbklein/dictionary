import org.junit.*;
import static org.junit.Assert.*;
import java.io.*;
import java.util.ArrayList;


public class WordTest {

  @After
  public void tearDown() {
    Word.clearWordArray();
  }

  @Test
  public void Word_instantiatesCorrectly_true(){
    Word testWord = new Word("Test");
    assertTrue(testWord instanceof Word);
  }

  @Test
  public void Word_instantiatesWithWordName_true(){
    Word testWord = new Word("Test");
    assertTrue(testWord.getWordName().equals("Test"));
  }

  @Test
  public void Word_instantiatesWithWordDefinitionAndName_true(){
    Definition testDefinition = new Definition("Example definition");
    Word testWord = new Word("Test", testDefinition);
    assertTrue(testWord.getDefinitions().size() == 1);
    assertEquals("Test", testWord.getWordName());
  }

  @Test
  public void Word_instantiatesWithID_1(){
    Word testWord = new Word("Test");
    assertEquals(1, testWord.getID());
  }

  @Test
  public void getAllWords_returnsAllWords_wordArray(){
    Word testWordOne = new Word("Test");
    Word testWordTwo = new Word("Test2");
    assertEquals(2, Word.getAllWords().size());
  }

  @Test
  public void clearWordArray_clearsWordArray_0(){
    Word testWordOne = new Word("Test");
    Word testWordTwo = new Word("Test2");
    Word.clearWordArray();
    assertEquals(0, Word.getAllWords().size());
  }

  @Test
  public void findByID_returnsCorrectWord_Word(){
    Word testWordOne = new Word("Test");
    Word testWordTwo = new Word("Test2");
    assertEquals(testWordOne, Word.findByID(1));
  }

  @Test
  public void findByID_returnsNullIfOutOfBounds_null(){
    assertEquals(null, Word.findByID(999));
  }

  @Test
  public void getWordName_returnsWordName_wordName(){
    Word testWord = new Word("wordName");
    String expected = "wordName";
    assertEquals(expected, testWord.getWordName());
  }

  @Test
  public void getID_returnsID_2(){
    Word testWordOne = new Word("Test");
    Word testWordTwo = new Word("Test2");
    assertEquals(2, testWordTwo.getID());
  }

  @Test
  public void getDefinitions_returnsDefinitionArray_DefinitionArray(){
    Definition testDefinition = new Definition("Example definition");
    Word testWord = new Word("Test", testDefinition);
    assertTrue(testWord.getDefinitions() instanceof ArrayList);
  }

  @Test
  public void addDefinition_addsDefinition_definition(){
    Definition testDefinition = new Definition("Definition");
    Word testWord = new Word("wordName", testDefinition);
    String expected = "Definition";
    String observed = testWord.getDefinitions().get(0).getText();
    assertEquals(expected, observed);
  }

}
